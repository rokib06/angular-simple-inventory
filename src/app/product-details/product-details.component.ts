import { Component, OnInit, Input } from '@angular/core';
import { ProductService } from '../product.service';
import { Product } from '../product';
 
import { ProductsListComponent } from '../products-list/products-list.component';
 




@Component({
  selector: 'product-details', 
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {
 
  @Input() product: Product;
 
  constructor(private productService: ProductService, private listComponent: ProductsListComponent) { }
 
  ngOnInit() {
  }
 
  updateActive(isActive: boolean) {
    this.productService.updateProduct(this.product.id,
      { title: this.product.title, description: this.product.description, price: this.product.price, amount:this.product.amount })
      .subscribe(
        data => {
          console.log(data);
          this.product = data as Product;
        },
        error => console.log(error));
  }

  getProduct() {
    this.productService.getProduct(this.product.id)
    
  }
 
  deleteProduct() {
    this.productService.deleteProduct(this.product.id)
      .subscribe(
        data => {
          console.log(data);
          this.listComponent.reloadData();
        },
        error => console.log(error));
  }
 
}
