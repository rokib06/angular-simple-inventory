import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
 
@Injectable({
  providedIn: 'root'
})



/*
<!---->
 | Angular 4200 | --> makes an API call --> | API Server 1234|
 | Angular 4200 | --> make an API call to /api/* --> webpack dev server --> |API Server 1234| 
-->
*/
export class ProductService {
 
  private baseUrl = '/api/product';
 
  constructor(private http: HttpClient) { }
 
  getProduct(id: number): Observable<object> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }
 
  createProduct(product: Object): Observable<object> {
    return this.http.post(`${this.baseUrl}/`, product);
  }
 
  updateProduct(id: number, value: any): Observable<object> {
    return this.http.put(`${this.baseUrl}/${id}/`, value);
  }
 
  deleteProduct(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`);
  }
 
  getProductsList(): Observable<any> {
    return this.http.get(`${this.baseUrl}/`);
  }
 
  
}