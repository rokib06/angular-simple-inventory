export class Product {
    id: number;
    title: string;
    description: string;
    amount: number;
    price: number;
}