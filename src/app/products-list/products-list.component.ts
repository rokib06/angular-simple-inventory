
import { Component, OnInit } from '@angular/core';
import { Observable, from } from 'rxjs';
 
import { ProductService } from '../product.service';
import { Product } from '../product';

// id routing 
import {Router} from '@angular/router';


 
@Component({
  selector: 'products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.css']
})
export class ProductsListComponent implements OnInit {
 
  products: Observable<Product[]>;

 
  constructor(private productService: ProductService,private _router: Router) { }
 
  ngOnInit() {
    this.reloadData();
  }
 

 
  reloadData() {
    this.products = this.productService.getProductsList();
  }
  onClick(productId: number ){
    // link parameter array
     this._router.navigate(['/product',productId]);
  }
 
}


/* import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.css']
})
export class ProductsListComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
*/